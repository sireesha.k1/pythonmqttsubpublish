from paho.mqtt import client as mqtt_client


class Mqtt_publishsubscribe():

    def __init__(self, config):
        self.config = config

    def run(self):
        try:
            obj = Subscribe(config)
            obj.run()
        except Exception as e:
            self.logger.exception("Some error occurred")

        self.write_data()


class Subscribe:
    def __init__(self, config):
        self.config = config
        self.broker1 = config['broker1']
        self.port = config['port']
        self.topic1 = config['read_topic']

    def connect_mqtt(self) -> mqtt_client:
        def on_connect(client, userdata, flags, rc):
            if rc == 0:
                print("Connected to MQTT Broker!")
            else:
                print("Failed to connect, return code %d\n", rc)

        client = mqtt_client.Client()
        client.on_connect = on_connect
        client.connect(self.broker1, self.port)
        return client

    def subscribe(self, client: mqtt_client):
        def on_message(client, userdata, msg):
            print(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")
            msg1 = msg.payload.decode()
            print(msg1)
            class_obj1 = Publishing(config, msg1)
            class_obj1.run()

        client.subscribe(self.topic1)
        client.on_message = on_message

    def run(self):
        client = Subscribe.connect_mqtt(self)
        self.subscribe(client)
        client.loop_forever()


class Publishing:

    def __init__(self, config, msg1):
        self.config = config
        self.broker2 = config['broker2']
        self.port = config['port']
        self.topic2 = config['write_topic']
        self.user_input = msg1

    def connect_mqtt(self):
        def on_connect(client, userdata, flags, rc):
            if rc == 0:
                print("Connected to MQTT Broker!")
            else:
                print("Failed to connect, return code %d\n", rc)

        client = mqtt_client.Client(self.user_input)
        client.on_connect = on_connect
        client.connect(self.broker2, self.port)
        return client

    def publish(self, client):
        msg = f"message :{self.user_input}"
        result = client.publish(self.topic2, msg)
        status = result[0]
        if status == 0:
            print(f" Send {self.user_input} to topic {self.topic2}")
        else:
            print(f"Failed to send message to topic {self.topic2}")

    def run(self):
        client = Publishing.connect_mqtt(self)
        client.loop_start()
        Publishing.publish(self, client)


config = {'broker1': '192.168.2.12', 'broker2': '192.168.2.12', 'port': 1883, 'read_topic': "housedoor",
          'write_topic': "output"}
obj = Mqtt_publishsubscribe(config)
obj.run()
