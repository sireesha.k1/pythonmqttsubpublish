from paho.mqtt import client as mqtt_client

config = {'broker': '192.168.2.12', 'port': 1883, 'topic': "mqtt"}


class MqttSubscribe:
    def __init__(self, config):
        self.broker = config['broker']
        self.port = config['port']
        self.topic = config['topic']

    def connect_mqtt(self) -> mqtt_client:
        def on_connect(client, userdata, flags, rc):
            if rc == 0:
                print("Connected to MQTT Broker!")
            else:
                print("Failed to connect, return code %d\n", rc)

        client = mqtt_client.Client()
        client.on_connect = on_connect
        client.connect(self.broker, self.port)
        return client

    def subscribe(self, client: mqtt_client):
        def on_message(client, userdata, msg):
            print(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")


        client.subscribe(self.topic)
        client.on_message = on_message

    def run(self):
        client = MqttSubscribe.connect_mqtt(self)
        self.subscribe(client)
        client.loop_forever()


if __name__ == '__main__':
    class_obj = MqttSubscribe(config)
    class_obj.run()

