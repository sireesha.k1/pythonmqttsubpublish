from paho.mqtt import client as mqtt_client

config = {'broker': '192.168.2.117', 'port': 1883, 'topic': "house_door12"}
config1 = {'broker': '192.168.2.12', 'port': 1883, 'topic': "housedoor"}


class Subscribe:
    def __init__(self, config):
        self.config = config
        self.broker = config['broker']
        self.port = config['port']
        self.topic = config['topic']

    def connect_mqtt(self) -> mqtt_client:
        def on_connect(client, userdata, flags, rc):
            if rc == 0:
                print("Connected to MQTT Broker!")
            else:
                print("Failed to connect, return code %d\n", rc)

        client = mqtt_client.Client()
        client.on_connect = on_connect
        client.connect(self.broker, self.port)
        return client

    def subscribe(self, client: mqtt_client):
        def on_message(client, userdata, msg):
            print(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")
            msg1 = msg.payload.decode()
            print(msg1)
            class_obj1 = Publishing(config1, msg1)
            class_obj1.run()

        client.subscribe(self.topic)
        client.on_message = on_message

    def run(self):
        client = Subscribe.connect_mqtt(self)
        self.subscribe(client)

        client.loop_forever()


class Publishing:

    def __init__(self, config1, msg1):
        self.config1 = config1
        self.broker = config1['broker']
        self.port = config1['port']
        self.topic = config1['topic']
        self.user_input = msg1

    def connect_mqtt(self):
        def on_connect(client, userdata, flags, rc):
            if rc == 0:
                print("Connected to MQTT Broker!")
            else:
                print("Failed to connect, return code %d\n", rc)

        client = mqtt_client.Client(self.user_input)
        client.on_connect = on_connect
        client.connect(self.broker, self.port)
        return client

    def publish(self, client):
        msg = f"message :{self.user_input}"
        result = client.publish(self.topic, msg)
        status = result[0]
        if status == 0:
            print(f" Send {self.user_input} to topic {self.topic}")
        else:
            print(f"Failed to send message to topic {self.topic}")

    def run(self):

        client = Publishing.connect_mqtt(self)

        client.loop_start()
        Publishing.publish(self, client)


if __name__ == '__main__':  
    obj = Subscribe(config)
    obj.run()