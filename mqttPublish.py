import time
import json
from paho.mqtt import client as mqtt_client

config = {'broker': '192.168.2.12', 'port': 1883, 'topic': "mqttconsumer",'user_input':[int(input("Enter a:")),int(input("Enter b:"))]}


class MqttPublish:
    def __init__(self, config):
        self.broker = config['broker']
        self.port = config['port']
        self.topic = config['topic']
        self.user_input = config['user_input']

    def connect_mqtt(self):
        def on_connect(client, userdata, flags, rc):
            if rc == 0:
                print("Connected to MQTT Broker!")

            else:
                print("Failed to connect, return code %d\n", rc)

        client = mqtt_client.Client(self.user_input)
        client.on_connect = on_connect
        client.connect(self.broker, self.port)
        return client

    def publish(self, client):
        jsondata={"var_a":self.user_input[0],"var_b":self.user_input[1]}
        # msg = f"message :{self.user_input}"
        msg=json.dumps(jsondata)
        time.sleep(1)
        result = client.publish(self.topic, msg)
        status = result[0]
        if status == 0:
            print(f" Send {self.user_input} to topic {self.topic}")
        else:
            print(f"Failed to send message to topic {self.topic}")

    def run(self):
        client = MqttPublish.connect_mqtt(self)
        client.loop_start()
        MqttPublish.publish(self, client)


if __name__ == '__main__':
    class_obj = MqttPublish(config)
    class_obj.run()
